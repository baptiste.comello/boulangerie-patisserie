import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { DescProduitComponent } from '../desc-produit/desc-produit.component';

@Component({
  selector: 'app-patisserie',
  templateUrl: './patisserie.component.html',
  styleUrls: ['./patisserie.component.css']
})
export class PatisserieComponent implements OnInit {

  constructor(    
    public dialog: MatDialog
    ) {
  }

  ngOnInit(): void {
  }

  descProduit(){
    const desProduit = new MatDialogConfig();
    desProduit.disableClose = true;
    desProduit.autoFocus=true;
    desProduit.width = "30%";
    desProduit.height = "530px"
    let modal = this.dialog.open(DescProduitComponent, desProduit);
    modal.afterClosed().subscribe(result => {
    });
  }

}
