import { Component, OnInit } from '@angular/core';


@Component({
  selector: 'app-basket',
  templateUrl: './basket.component.html',
  styleUrls: ['./basket.component.css']
})


export class BasketComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  displayedColumns: string[] = ['Article', 'Quantité', 'Prix', 'UpdateDelete'];
  data: string[] = ['Brioche', 'Tartelette à la fraise', 'Croissant', 'Tarte normande', 'Baguette'];
}
