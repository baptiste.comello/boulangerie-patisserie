import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BoulangerieComponent } from './boulangerie/boulangerie.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LayoutModule } from '@angular/cdk/layout';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { MatCardModule } from '@angular/material/card';
import { MatMenuModule } from '@angular/material/menu';
import {MatDialogModule} from '@angular/material/dialog';
import { DescProduitComponent } from './desc-produit/desc-produit.component';
import { BasketComponent } from './basket/basket.component';
import { MatTableModule } from '@angular/material/table';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { PatisserieComponent } from './patisserie/patisserie.component';
import { TraiteurComponent } from './traiteur/traiteur.component';

const appRoutes: Routes = [
    { path :'home', component: HomeComponent},
    { path: 'boulangerie', component: BoulangerieComponent },
    { path: 'patisserie', component: PatisserieComponent },
    { path: 'basket', component: BasketComponent },
]

@NgModule({
  declarations: [
    AppComponent,
    BoulangerieComponent,
    DescProduitComponent,
    BasketComponent,
    HomeComponent,
    PatisserieComponent,
    TraiteurComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    MatCardModule,
    MatMenuModule,
    MatDialogModule,
    MatTableModule,
    HttpClientModule,
    RouterModule.forRoot(appRoutes)
  ],

  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
