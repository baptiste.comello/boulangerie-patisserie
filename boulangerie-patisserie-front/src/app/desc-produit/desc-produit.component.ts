import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { Inject } from '@angular/core';

@Component({
  selector: 'app-desc-produit',
  templateUrl: './desc-produit.component.html',
  styleUrls: ['./desc-produit.component.css']
})
export class DescProduitComponent implements OnInit {

  constructor(    
    public dialogRef : MatDialogRef<DescProduitComponent>
  )
  { }

  ngOnInit(): void {
  }


  cancel(){
    this.dialogRef.close();
  }
}
