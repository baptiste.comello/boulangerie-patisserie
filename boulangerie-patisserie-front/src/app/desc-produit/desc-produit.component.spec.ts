import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DescProduitComponent } from './desc-produit.component';

describe('DescProduitComponent', () => {
  let component: DescProduitComponent;
  let fixture: ComponentFixture<DescProduitComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DescProduitComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DescProduitComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
